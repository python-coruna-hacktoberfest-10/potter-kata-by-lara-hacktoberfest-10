
class Basket():
    def __init__(self, pedido):
        if len(pedido)==5:
            self.catalog = pedido
        else:
            raise ValueError
        
        self.discounts = {
            2: 0.95,
            3: 0.9,
            4: 0.8,
            5: 0.75
        }

    def calcula_precio(self):
        lista_pedido = list(self.catalog.values())
        
        
        subtotales=[]

        while sum(lista_pedido) > 0:
            tamano_set = 0
            for idx, libros in enumerate(lista_pedido):
                if libros > 0:
                    tamano_set +=1
                    lista_pedido[idx] = libros-1
            
            subtotal = tamano_set*8
            
            # if tamano_set == 2:
            #     subtotal = subtotal*0.95
            if tamano_set in self.discounts:
                subtotal = subtotal*self.discounts[tamano_set]

            subtotales.append(subtotal)

        total = sum(subtotales)

        return total
        