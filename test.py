import unittest
import numpy as np
from potter import Basket

class BasketTest(unittest.TestCase):

    def test_devuelve_cantidad_de_libros(self):
        pedido = self.sample_basket()
        canasta = Basket(pedido)
        self.assertEqual(len(canasta.catalog.keys()), 5)

    def test_create_new_basket(self):
        pedido = self.sample_basket()
        canasta = Basket(pedido)
        self.assertEqual(canasta.catalog, pedido)

    def test_price_of_one_book(self):
        pedido = self.sample_basket([1,0,0,0,0])
        canasta = Basket(pedido)
        self.assertEqual(canasta.calcula_precio(), 8)

    def test_price_of_two_equal_books(self):
        pedido = self.sample_basket([2,0,0,0,0])
        canasta = Basket(pedido)
        self.assertEqual(canasta.calcula_precio(), 16)

    def test_price_of_two_distinct_books(self):
        pedido = self.sample_basket([1,1,0,0,0])
        canasta = Basket(pedido)
        precio_correcto = 2*8*0.95
        self.assertEqual(canasta.calcula_precio(), precio_correcto)

    def test_price_of_empty_basket(self):
        pedido = self.sample_basket([0,0,0,0,0])
        canasta = Basket(pedido)
        self.assertEqual(canasta.calcula_precio(), 0)

    def test_price_of_three_distinct_books(self):
        pedido = self.sample_basket([1,1,1,0,0])
        canasta = Basket(pedido)
        precio_correcto = 3*8*0.9
        self.assertEqual(canasta.calcula_precio(), precio_correcto)

    def test_price_of_several_sets(self):
        pedido = self.sample_basket([1,2,2,1,1])
        canasta = Basket(pedido)
        precio_correcto = 5*8*0.75 + 2*8*0.95
        self.assertEqual(canasta.calcula_precio(), precio_correcto)

    @unittest.expectedFailure
    def test_throw_error_at_big_basket(self):
        pedido = self.sample_basket([1,0,0,0,0,0,0,0,0,0,0])
        Basket(pedido)


    def sample_basket(self, lista_pedido = np.random.randint(10, size=5)):
        return {idx:item for idx, item in enumerate(lista_pedido)}
               

if __name__ == '__main__':
    unittest.main()
